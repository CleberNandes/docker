# docker
Execute a docker image, if doesn't existe this download
`docker run hello-world`

AULA 02

List images that was downloaded
`docker image ls`

List the containers
`docker container ls --all`

List images all containers including stopped
`docker ps -a`

list just containers active
`docker ps`

when execute `docker run` it always create a new image

interact with the terminal from the image ubuntu created
`docker run -it ubuntu`

a container can be running(start) or stopped(stop)
start a container
`docker start container_id`

stop a containers
`docker stop container_id`

no wait a container be stopped pass -t 0 flag
`docker stop -t 0 container_id`

To stop a container we just need to pass the begining character of container_id or container_name
we also can stop a container by its name
`docker stop container_name`

start a container using ubuntu terminal without create
`docker start -a -i container_id`

remove a container
`docker rm container_id`

remove all container inactive
`docker container prune`

remove a image
`docker rmi image_name_repository`

all image you download has more than a layer
this is to share with others images
layers are read only
to write and ubuntu the image create a new thin layer
an image that is used by more than one containner is shared between them
to run a image from an user we have to pass a user/image always
just the official images don't need inform a user before image

create a Container that hold a static site
`docker run dockersamples/static-site`

the container above locks the terminal when executed
to avoid this locks we should pass a flag -d
`-d` = detached
it let the termial free to keep using
`docker run -d dockersamples/static-site`

`ctrl + c` stop a container that was runned without -d flag

To has a port to acess to comunicate with the container's port 80
we has to use -P flag
`docker run -d -P dockersamples/static-site`
it'll handle a chosen port to us

`docker ps` to see which port to access.
We have to use the port that precedes container's main port 80

lanch `localhost:32769` to access the docker's welcome site

another way to see the ports is to used
`docker port container_id`
`docker port 236b46d1d60b`
443/tcp -> 0.0.0.0:32768
80/tcp -> 0.0.0.0:32769
we have to use the 80/tcp

0.0.0.0 = localhost

wen creating a container we can give its name with flag --name
`docker run -d -P --name my-site dockersamples/static-site`

give a specific port to use docker - flag -p port - p has to be tiny
`docker run -d -p 8081:80 --name my-site dockersamples/static-site`

always when stop a container - if used -P (capital) to create it -the port change every single time when start it again

env variables `-e VARIABLE=VALUE`
to set a env variable it is just pass it and its value in the docker run command
`docker run -d -P -e AUTHOR="Cleber Fernandes" dockersamples/static-site`

return just containers ids that are running `-q`
`docker ps -q`

stop all containres through a $(docker ps -q)
`docker stop $(docker ps -q)`

AULA 03 - Docker host - volumes

o volume fica no Docker Host. Ou seja, fica salvo no computador onde a Docker Engine está rodando.

sharing a directory with the container C:\my-pc-dir:/container-dir/
create a container with a volumes`-v`
`docker run -it -v "C:\my-pc-dir:/container-dir/" ubuntu`

when delete a container this way we don't loose the data because it is being saving in our pc

inpect the container
`docker inpect container_id`

after inspect to see the volume used look at

Run a source code local on the container in nodejs.
First run a container that has node installed
`docker run -v "C:\Users\RE036609\git\docker\volume-exemplo:/var/www" node`

passing a command to docker runned
`docker run -v "C:\Users\RE036609\git\docker\volume-exemplo:/var/www" node npm start`

the source code is map to access port 3000
`docker run -p 8080:3000 -v "C:\Users\RE036609\git\docker\volume-exemplo:/var/www" node npm start`

work directory to initiate the container
-w /var/www
`docker run -d -p 8080:3000 -v "C:\Users\RE036609\git\docker\volume-exemplo:/var/www" -w /var/www node npm start`

if Im in the work directory I can pass a command interpolation through
`$(pwd)`
`docker run -d -p 8080:3000 -v "$(pwd):/var/www" -w /var/www node npm start`

AULA 04 - DOCKERFILE - Build our own images

stop and remove a container
`docker rm -f container_id`

before you create images y0u have to create a DOCKERFILE in app root
it build a image statrting from another existing image
we can create a imge from scratch but it's is more usual build startingo from existing onde

writing DOCKERFILE
FROM
`FROM node` it is the same thing that `FROM node:latest`
we can specify a version of the existing image by adding `:"version"`
if we do not specify it always will get the latest version

MAINTAINER
Who keep the images
`MAINTAINER Cleber Fernandes`

COPY
`COPY . /var/www`

WORKDIR - tell wich directory the container should iniciate
`WORKDIR /var/www`

RUN
`RUN npm install`

ENTRYPOINT - is the command that will be execute when the container is iniciate
`ENTRYPOINT npm start`

Expose - tell which port can be used
`EXPOSE 3000`

Build DOCKERFILE
To build a image starting from a DOCKERFILE
`docker build -f file -t tagname`
`-f file` if the file name is Dockerfile it needn't be specified
`-t user/image` inform the image name we going to build

Create a container starting from a builded image
`docker run -d -p 8080:3000 clebera/node-docker .`
the dot "." at the final of the command is necessary to use the Dockerfile from the local directory

if we are behind a proxy we must pass another command to our Dockerfile
`ENV http_proxy='http://re036609:Cl3b3ran@192.168.200.246:3128'`
`ENV https_proxy='http://re036609:Cl3b3ran@192.168.200.246:3128'`

after build a image `docker images` to see if the image was really created

create a container starting from the created image
`docker run `
